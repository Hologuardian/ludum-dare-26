package lifebloom.com.map;

import java.util.ArrayList;

import lifebloom.com.entity.EntityFactory;
import lifebloom.com.entity.EntityLiving;
import lifebloom.com.entity.EntityPlayer;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

public class World 
{
	ArrayList<EntityLiving> entityList = new ArrayList<EntityLiving>();
	public EntityPlayer entityPlayer;
	public int difficultyLevel = 2;
	ArrayList<EntityLiving> entitiesToRemove = new ArrayList<EntityLiving>();
	public StateBasedGame game;
	
	public World(GameContainer gc, StateBasedGame game)
	{
		entityPlayer = EntityFactory.generatePlayer(2, gc, this);
		entityList.add(entityPlayer);
		this.game = game;
	}
	
	public void update(GameContainer gc, int delta)
	{
		if (EntityFactory.entityRandom.nextInt(10000) == 0)
		{
			entityList.add(EntityFactory.generateLivingEntity(this.difficultyLevel, this, gc));
		}
		
		for(EntityLiving entity : entitiesToRemove)
		{
			entityList.remove(entity);
		}

		entitiesToRemove.clear();
		
		for(EntityLiving entity : entityList)
		{
			if (entity != null && entity != entityPlayer)
			{
				entity.update(gc, delta);
				for (EntityLiving entityB : entityList)
				{
					if (entityB != null && entityB != entity)
						checkCollision(entity, entityB);
				}
			}
		}
	}
	
	public void updateRenders(Graphics g)
	{
		for(EntityLiving entity : entityList)
		{
			if (entity != null)
				entity.updateRender(g);
		}
	}
	
	public void addEntity(EntityLiving entity)
	{
		entityList.add(entity);
	}
	
	public boolean checkCollision (EntityLiving entA, EntityLiving entB)
	{
		int i = 0;
		int j = 0;
		for (Shape modA : entA.getBB())
		{
			if (modA != null)
			{
				for (Shape modB : entB.getBB())
				{
					if (modB != null)
					{
						float x = modA.getCenterX() - modB.getCenterX();
						float y = modA.getCenterY() - modB.getCenterY();
						float len = (float) Math.sqrt(x*x + y*y);
						if (len < modA.getBoundingCircleRadius() + modB.getBoundingCircleRadius())
						{
							if (entA.getModuleFromBBPosition(i) != null && entB.getModuleFromBBPosition(j) != null)
							{
								entA.onCollideWithEntity(entB, entA.getModuleFromBBPosition(i), entB.getModuleFromBBPosition(j));
								entB.onCollideWithEntity(entA, entB.getModuleFromBBPosition(j), entA.getModuleFromBBPosition(i));
								return true;
							}
						}
					}
					j++;
				}
			}
			j = 0;
			i++;
		}
		return false;
	}
	
	public void removeEntity(EntityLiving entity)
	{
		this.entitiesToRemove.add(entity);
		this.difficultyLevel++;
	}
}