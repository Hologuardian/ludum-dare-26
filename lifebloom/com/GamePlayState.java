package lifebloom.com;

import lifebloom.com.map.World;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class GamePlayState extends BasicGameState
{
	int stateID;
	World world;
	
	public GamePlayState(int state)
	{
		stateID = state;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		world = new World(gc, game);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		world.updateRenders(g);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		world.update(gc, delta);
		world.entityPlayer.update(gc, game, delta);
	}

	@Override
	public int getID() {
		return stateID;
	}

}
