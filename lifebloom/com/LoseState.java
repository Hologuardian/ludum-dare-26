package lifebloom.com;

import lifebloom.com.map.World;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class LoseState extends BasicGameState
{
	int stateID;
	World world;
	Image image;
	
	public LoseState(int state)
	{
		stateID = state;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		image = new Image("res/sprite/GameOver.png");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		image.draw(0, 0, gc.getWidth(), gc.getHeight());
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
	}

	@Override
	public int getID() {
		return stateID;
	}

}