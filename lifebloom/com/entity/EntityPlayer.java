package lifebloom.com.entity;

import java.util.Random;

import lifebloom.com.LifebloomMain;
import lifebloom.com.map.World;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.FastTrig;

public class EntityPlayer extends EntityLiving
{
	public EntityPlayer(Random rand, EnumTrait[] prop, GameContainer gc, World world) {
		super(rand, prop, world, gc);
		this.posX = gc.getWidth() / 2;
		this.posY = gc.getHeight() / 2;
	}
	
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		Input input = gc.getInput();

		double x = 0;
		double y = 0;
		if (input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON))
		{
			this.setAttacking(input.getMouseX(),input.getMouseY(), false);
		}
		
		if (this.isAttacking)
		{
			x = (this.targetX - this.posX) * 2;
			y = (this.targetY - this.posY) * 2;
		}
		else
		{
			if (input.isKeyDown(Input.KEY_W))
			{
				y -= 100;
			}
			if (input.isKeyDown(Input.KEY_A))
			{
				x -= 100;
			}
			if (input.isKeyDown(Input.KEY_S))
			{
				y += 100;
			}
			if (input.isKeyDown(Input.KEY_D))
			{
				x += 100;
			}
		}
		
		if (x !=0 && y != 0)
		{
			x = x / (100 / Math.sqrt(5000));
			y = y / (100 / Math.sqrt(5000));
		}
		
		double theta = StrictMath.atan2(y, x);
		theta += Math.PI / 2;
		float len = (float) Math.sqrt(x*x +y*y);
		

		int speed = (7) * delta;
		
		
		if (len < 10)
			rotation += 0.002 * delta;
		else if (Math.sqrt((rotation - theta) * (rotation - theta)) < 0.02){}
		else
		{
			if (theta > Math.PI && (rotation < Math.PI / 4|| rotation > Math.PI * 9 / 4))
				rotation -= 0.004 * delta;
			else if (rotation < theta 
					|| (rotation > Math.PI  && theta < Math.PI / 4))
				rotation += 0.004 * delta;
			else
				rotation -= 0.004 * delta;
		}
		
		if (this.velX < -len * (float) FastTrig.cos(theta) * this.maxSpeedMod * speed)
			this.velX += this.speedMod * speed;
		else this.velX -= this.speedMod * speed;
		
		if (this.velY < len * (float) FastTrig.sin(theta) * this.maxSpeedMod * speed)
			this.velY += this.speedMod * speed;
		else this.velY -= this.speedMod * speed;
		
		update(gc, delta);
	}
	
	@Override
	public boolean isAIEnabled()
	{
		return false;
	}

	public EnumTrait wither(Module mod)
	{
		if (mod.trait.ordinal() == EnumTrait.base.ordinal())
		{
			this.worldObj.game.enterState(LifebloomMain.LOSE_STATE);
		}
		return super.wither(mod);
	}
}
