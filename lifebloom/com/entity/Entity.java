package lifebloom.com.entity;

import java.util.Random;

import org.newdawn.slick.GameContainer;

public class Entity 
{
	protected float posX = 0;
	protected float posY = 0;
	protected double velX = 0;
	protected double velY = 0;
	protected Random rand;
	
	public Entity(Random rand)
	{
		this.rand = rand;
	}
	
	public void update(GameContainer gc, int delta)
	{
		System.out.println(this.checkBorderCollision(gc));
		posX += this.velY;
		posY += this.velX;
	}
	
	public boolean checkBorderCollision(GameContainer gc)
	{
		return false;
	}
}
