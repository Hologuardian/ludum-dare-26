package lifebloom.com.entity;

import java.util.Random;

import lifebloom.com.map.World;

import org.newdawn.slick.GameContainer;

public class EntityFactory 
{
	public static Random entityRandom = new Random(System.currentTimeMillis());
	
	public static EntityLiving createLivingEntity(EnumTrait[] traits, World world, GameContainer gc)
	{
		EnumTrait[] properties = EnumTrait.values();
		
		for(EnumTrait trait : traits)
		{
			properties[trait.ordinal()].value++;
		}

		return makeLivingEntity(properties, world, gc);
	}
	
	public static EntityLiving generateLivingEntity(int skill, World world, GameContainer gc)
	{
		int points = skill;
		EnumTrait[] properties = EnumTrait.values();
		while(points > 0)
		{
			int traitType = entityRandom.nextInt(10) + 1;
			int cost = EnumTrait.values()[traitType].getCost();
			if (points >= cost)
			{
				properties[traitType].value++;
				points -= cost;
			}
		}
		
		return makeLivingEntity(properties, world, gc);
	}
	
	public static EntityPlayer generatePlayer(int skill, GameContainer gc, World world)
	{
		int points = skill;
		EnumTrait[] properties = EnumTrait.values();
		while(points > 0)
		{
			int traitType = entityRandom.nextInt(10) + 1;
			int cost = EnumTrait.values()[traitType].getCost();
			if (points >= cost)
			{
				properties[traitType].value++;
				points -= cost;
			}
		}

		EntityPlayer entity = new EntityPlayer(entityRandom, properties, gc, world);
		entity.generateEntity(properties);
		
		return entity;
	}
	
	private static EntityLiving makeLivingEntity(EnumTrait[] prop, World world, GameContainer gc)
	{
		EntityLiving entity = new EntityLiving(entityRandom, prop, world, gc);
		entity.generateEntity(prop);
		
		return entity;
	}
}
