package lifebloom.com.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import lifebloom.com.map.World;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.util.FastTrig;

public class EntityLiving extends Entity
{
	public final double speedMod = 0.00007;
	public final double maxSpeedMod = 0.0003;
	float size = 20;
	protected int[] properties = {0,0,0,0,0,0,0,0,0,0,0};
	Module origin = null;
	float rotation;
	protected int attackTimer = 0;
	protected static final int attackDelay = 500;
	protected int attackCoolDown = 0;
	public boolean isAttacking;
	public float targetX;
	public float targetY;
	public Shape[] bb = new Shape[32];
	private boolean isForcedMoving = false;
	private int forceMoveTimer = 0;
	private double fvx = 0;
	private double fvy = 0;
	private boolean isMoving;
	protected World worldObj;

	public EntityLiving(Random rand, EnumTrait[] prop, World world, GameContainer gc)
	{
		super(rand);
		this.worldObj = world;
		for(EnumTrait proper : prop)
		{
			properties[proper.ordinal()] = proper.value;
		}
		origin = new Module(EnumModClasses.origin, null, EnumTrait.base);
		this.posX = this.rand.nextInt(gc.getWidth());
		this.posY = this.rand.nextInt(gc.getHeight());

	}

	public int[] getProperties()
	{
		return this.properties;
	}

	public void setAttacking(float x, float y, boolean override)
	{
		if (this.attackCoolDown == 0 || override)
		{
			this.isAttacking = true;
			this.targetX = x;
			this.targetY = y;
		}
	}

	public void setMoving(float x, float y)
	{
		this.isMoving = true;
		this.targetX = x;
		this.targetY = y;
	}

	public void update(GameContainer gc, int delta)
	{
		if (!this.isForcedMoving)
		{
			super.update(gc, delta);
		}
		else
		{
			if (forceMoveTimer > 0)
				forceMoveTimer--;
			else
				this.isForcedMoving = false;

			this.posX += this.fvx;
			this.posY += this.fvy;
			this.velY = this.fvx;
			this.velX = this.fvy;
		}

		if(rotation > Math.PI * 3/2)
			rotation = (float) ((rotation % (Math.PI * 3/2 )) - Math.PI/2);
		else if(rotation < -Math.PI /2)
			rotation += Math.PI * 2;

		if (this.isAIEnabled())
		{
			if (!this.isAttacking && this.rand.nextInt(1000) == 0)
			{
				this.setAttacking(worldObj.entityPlayer.posX, worldObj.entityPlayer.posY, false);
			}
			
			
			if (!this.isMoving && !this.isAttacking)
			{
				this.setMoving(this.rand.nextInt(gc.getWidth() - 100) + 50, this.rand.nextInt(gc.getHeight() - 100) + 50);
			}
			else
			{

				double x = 0;
				double y = 0;
				if (this.isMoving)
				{
					x = this.targetX - this.posX;
					y = this.targetY - this.posY;
				}

				if (x !=0 && y != 0)
				{
					x = x / (100 / Math.sqrt(5000));
					y = y / (100 / Math.sqrt(5000));
				}

				double theta = StrictMath.atan2(y, x);
				theta += Math.PI / 2;
				float len = (float) Math.sqrt(x*x +y*y);

				if (len < 20)
				{
					this.isMoving = false;
				}

				int speed = (4) * delta;


				if (len < 10)
					rotation += 0.002 * delta;
				else if (Math.sqrt((rotation - theta) * (rotation - theta)) < 0.02){}
				else
				{
					if (theta > Math.PI && (rotation < Math.PI / 4|| rotation > Math.PI * 9 / 4))
						rotation -= 0.004 * delta;
					else if (rotation < theta 
							|| (rotation > Math.PI  && theta < Math.PI / 4))
						rotation += 0.004 * delta;
					else
						rotation -= 0.004 * delta;
				}

				if (this.velX < -len * (float) FastTrig.cos(theta) * maxSpeedMod * speed)
					this.velX += speedMod * speed;
				else this.velX -= speedMod * speed;

				if (this.velY < len * (float) FastTrig.sin(theta) * maxSpeedMod * speed)
					this.velY += speedMod * speed;
				else this.velY -= speedMod * speed;
			}
		}

		if (this.isAttacking)
		{
			attackTimer--;
			if (attackTimer <= 0)
			{
				this.isAttacking = false;
				this.attackTimer = attackDelay;
				this.attackCoolDown = 500;
			}
		}
		else if (this.attackCoolDown > 0)
			this.attackCoolDown--;
	}

	public void onCollideWithEntity(EntityLiving other, Module ownModule, Module otherModule)
	{
		if (ownModule != null && other != null && otherModule != null)
		{
			if (this.isAttacking && !other.isAttacking)
			{
				other.velX = Double.valueOf(this.velX);
				other.velY = Double.valueOf(this.velY);
				EnumTrait oMod = other.wither(otherModule);
				if (oMod.ordinal() != EnumTrait.base.ordinal())
					this.origin.addModule(oMod);
				this.attackTimer = 0;
				this.setForceMove(100, this.velY, this.velX);
				this.velX *= -1;
				this.velY *= -1;
			}
			else if (!this.isAttacking && !other.isAttacking)
			{
				this.velX = (other.posX - this.posX) * 0.005;
				this.velY = (other.posY - this.posY) * 0.005;
				other.velX = (this.posX - other.posX) * 0.005;
				other.velY = (this.posY - other.posY) * 0.005;
				this.attackTimer = 0;
				other.attackTimer = 0;
			}
			else if (this.isAttacking && other.isAttacking)
			{
				this.velX = (other.posX - this.posX) * 0.005;
				this.velY = (other.posY - this.posY) * 0.005;
				other.velX = (this.posX - other.posX) * 0.005;
				other.velY = (this.posY - other.posY) * 0.005;
				this.attackTimer = 0;
				other.attackTimer = 0;
			}
		}
	}

	public boolean isAIEnabled()
	{
		return true;
	}

	public void updateRender(Graphics g)
	{
		g.setLineWidth(1);
		g.setAntiAlias(true);
		Arrays.fill(bb, null);
		drawModule(g, origin, this.posX, this.posY, 0, rotation, this.bb);

	}

	public Shape[] drawModule(Graphics g, Module module, float posX, float posY, double angle, float rotate, Shape[] bb)
	{
		if (module == null)
			return bb;
		
		
		g.setColor(module.trait.getColor());
		
		float angleModify;
		if (module.slots[0] == null)
			angleModify = 0;
		else if (module.slots[0].trait.ordinal() == EnumTrait.base.ordinal())
		{
			angleModify = (float) (((Math.PI * 2) * module.superSlot) / (module.slots[0].modClass.modClass() - 1));
			angleModify += rotate;
		}
		else
			angleModify = (float) (angle - (((Math.PI * 2) * module.superSlot) / module.slots[0].modClass.modClass())/2 + Math.PI/2);
		
		int xMod = (int)((size) * Math.cos(angleModify));
		int yMod = (int)((size) * Math.sin(angleModify));

		Ellipse graphic = null;
		
		if (module.trait.ordinal() == EnumTrait.base.ordinal())
		{
			xMod = 0;
			yMod = 0;
			graphic = new Ellipse(posX, posY, size/2, size / 2);
		}
		else
			graphic = new Ellipse(posX + xMod, posY + yMod, size / 4, size / 2);
		
		Shape newGraphic = graphic.transform(Transform.createRotateTransform((float) (angleModify + Math.PI/2), posX + xMod, posY + yMod));
		g.draw(newGraphic);
		
		newGraphic.setCenterX(posX + xMod);
		newGraphic.setCenterY(posY + yMod);
		bb[getBBPosition(module)] = newGraphic;
		
		int i = 0;
		for(Module mod : module.slots)
		{
			if (mod != null && i > 0)
			{
				bb = (drawModule(g, mod, (posX) + xMod, posY + yMod, angleModify, rotate, bb));
			}
			i++;
		}
		
		return bb;
	}

	public void absorbBloom(EnumTrait trait)
	{
		properties[trait.ordinal()]++;
		origin.addModule(trait);
	}

	public EnumTrait wither(Module mod)
	{
		if (mod.trait.ordinal() == EnumTrait.base.ordinal())
		{
			this.worldObj.removeEntity(this);
			return EnumTrait.base;
		}
		int prop = mod.trait.ordinal();
		properties[prop]--;
		this.bb[this.getBBPosition(mod)] = null;
		return mod.getSlot(0).removeModule(mod.superSlot);
	}

	public EntityLiving split(GameContainer gc)
	{
		EntityLiving entity = null;

		if(properties[EnumTrait.divider.ordinal()] > 0)
		{
			for (Module module : origin.getSlots())
			{
				if (module != null && (module.trait.ordinal() == EnumTrait.divider.ordinal()));
				{
					properties[EnumTrait.divider.ordinal()]--;
					EnumTrait[] entityTraits= (EnumTrait[]) origin.removeBranch(module.superSlot).getBranch(new ArrayList<EnumTrait>()).toArray();
					entity = EntityFactory.createLivingEntity(entityTraits, this.worldObj, gc);
					removeTraits(entityTraits);
				}
			}
		}

		return entity;
	}

	public Module generateEntity(EnumTrait[] entityProperties)
	{
		EnumTrait[] prop = entityProperties.clone();
		for(EnumTrait trait : prop)
		{
			while(trait.value > 0)
			{
				origin.addModule(trait);
				trait.value--;
			}
		}
		return origin;
	}

	public void removeTraits(EnumTrait[] traits)
	{
		for(EnumTrait trait : traits)
		{
			properties[trait.ordinal()]--;
		}
	}

	public Shape[] getBB()
	{
		return this.bb;
	}

	public Module getModuleFromBBPosition(int position)
	{
		Module module = origin;
		if (position <= 6 && position > 0)
		{
			module = origin.getSlot(position);
		}
		else if (position != 0)
		{
			int originSlot = 0;
			if ((position - 7) % 4 < 2)
			{
				originSlot = (int) (Math.floor((position - 7) / 4)) + 1;
				if (module.slots[originSlot] != null)
				{
					module = origin.getSlot(originSlot);
					if (module.slots[(((position - 7) % 4) + 1)] != null)
						module = module.getSlot(((position - 7) % 4) + 1);
				}
			}
			else
			{
				originSlot = (int) (Math.floor((position - 9) / 4)) + 1;
				if (module.slots[originSlot] != null)
				{
					module = origin.getSlot(originSlot);
					if (module.slots[((position - 9) % 4) + 1] != null)
						module = module.getSlot(((position - 9) % 4) + 1);
				}
			}
		}

		return module;
	}

	public int getBBPosition(Module module)
	{
		Module aboveModule = module;
		int ret = 0;
		int level = 0;
		while (aboveModule.superSlot != 0)
		{
			level++;
			aboveModule = aboveModule.slots[0];
		}

		switch(level)
		{
		case 0:
			ret = 0;
			break;
		case 1:
			ret = module.superSlot;
			break;
		case 2:
			ret = module.superSlot + (module.slots[0].superSlot - 1) * 4 + 7;
			break;
		case 3:
			ret = module.superSlot + (module.slots[0].superSlot - 1) * 4 + 9;
			break;
		default:
			ret = 31;
			break;
		}
		return ret;
	}

	@Override
	public boolean checkBorderCollision (GameContainer gc)
	{
		for (Shape mod : this.getBB())
		{
			if (mod != null)
			{
				float x = gc.getWidth() - mod.getCenterX();
				float y = gc.getHeight() - mod.getCenterY();
				float r = mod.getBoundingCircleRadius();
				if ((x - r < 0))
				{
					this.setForceMove(150, 0.1, 0);
					return true;
				}
				else if ((x + r> gc.getWidth()))
				{
					this.setForceMove(150, -0.1, 0);
					return true;
				}
				else if ((y - r < 0))
				{
					this.setForceMove(150, 0, 0.1);
					return true;
				}
				else if((y + r> gc.getHeight()))
				{
					this.setForceMove(150,  0, -0.1);
					return true;
				}
			}
		}
		return false;
	}

	public void setForceMove(int time, double vX, double vY)
	{
		this.isForcedMoving = true;
		this.forceMoveTimer = time;
		this.fvx = -vX;
		this.fvy = -vY;
	}

	/**
	 * Basically a way of organizing different traits for the entity
	 * Recursive
	 */
	class Module
	{
		EnumModClasses modClass;
		//The number of slots in a single module
		Module[] slots;
		public EnumTrait trait;
		public int superSlot;
		public int health;

		public Module(EnumModClasses modSize, Module origin, EnumTrait trait)
		{
			this.health = 5;
			this.trait = trait;
			//The first slot is always the Module that created the current one.
			slots = new Module[modSize.modClass()];
			modClass = modSize;

			if (origin != null)
				slots[0] = origin;

		}

		//Set a single slot in the Module
		public void setSlot(int slot, Module mod)
		{
			slots[slot] = mod;
			mod.superSlot = slot;
		}

		//This method creates a module from a trait and adds it to itself.
		public Module createModule(int slot, EnumTrait trait)
		{
			EnumModClasses modSize;
			if (modClass.ordinal() < EnumModClasses.end.ordinal())
			{
				modSize = EnumModClasses.fromOrdinal(modClass.ordinal() + 1);
				Module module = new Module(modSize, this, trait);
				this.setSlot(slot, module);
				return module;
			}
			else
				return null;

		}

		//Returns the Module attached to a specific slot, 
		public Module getSlot(int slot)
		{
			return slots[slot];
		}

		//removes all modules along a specific path
		public Module removeBranch(int slot)
		{
			Module mod = slots[slot];
			slots[slot] = null;
			return mod;
		}

		public Module[] getSlots()
		{
			return slots;
		}

		public ArrayList<EnumTrait> getBranch(ArrayList<EnumTrait> branch)
		{
			branch.add(this.trait);
			int i = 0;
			for(Module module : slots)
			{
				if (module != null && i > 0)
					branch = module.getBranch(branch);
				i++;
			}
			return branch;
		}

		public EnumTrait removeModule(int slot)
		{
			Module mod = slots[slot];
			removeBranch(slot);

			if (mod != null)
			{
				ArrayList<EnumTrait> branch = new ArrayList<EnumTrait>();
				ArrayList<EnumTrait> modulesToAdd;
				if (new ArrayList<EnumTrait>(mod.getBranch(branch)) != null);
				{
					modulesToAdd = new ArrayList<EnumTrait>(mod.getBranch(new ArrayList<EnumTrait>()));
					modulesToAdd.remove(0);

					for(EnumTrait module : modulesToAdd)
					{
						if (module != null)
							this.addModule(module);
					}

					return mod.trait;
				}
			}


			return EnumTrait.base;
		}

		//What decided what modules go where
		public void addModule(EnumTrait trait)
		{
			int j = 0;
			for (Module i : slots)
			{
				if (i == null)
				{
					createModule(j, trait);
					return;
				}
				else if (i.trait.ordinal() > trait.ordinal())
				{
					Module temp = i;
					Module mod = createModule(j, trait);
					if (mod.slots.length > 1)
					{
						slots[j].setSlot(1, temp);
					}
					return;
				}
				j++;
			}

			if (modClass.ordinal() < EnumModClasses.end.ordinal())
				slots[rand.nextInt(slots.length - 1) + 1].addModule(trait);
		}
	}
}
