package lifebloom.com.entity;

import org.newdawn.slick.Color;

public enum EnumTrait{
	base(0, Color.white),
	divider(1, Color.yellow),
	speed(1, Color.cyan),
	propagate(1, Color.orange),
	ranged(1, Color.magenta),
	wither(1, Color.darkGray),
	bloom(1, Color.green),
	planter(1, Color.blue),
	resist(1, Color.lightGray),
	aborber(1, Color.pink),
	spike(1, Color.red);
	public int value;
	private int cost;
	private Color color;
	
	EnumTrait(int cost, Color color)
	{
		this.setCost(cost);
		this.setColor(color);
	}
	
	public int getCost() {
		return cost;
	}
	
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
}
