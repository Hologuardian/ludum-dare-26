package lifebloom.com.entity;

public enum EnumModClasses{
	origin(7),
	large(3),
	medium(2),
	end(1);
	private final int modules;
	
	EnumModClasses(int mods)
	{
		modules = mods;
	}
	
	public static EnumModClasses fromOrdinal(int ordinal)
	{
		switch(ordinal)
		{
		case 0:
			return origin;
		case 1:
			return large;
		case 2:
			return medium;
		case 4:
			return end;
		default:
			return end;
		}
	}
	
	public int modClass(){ return modules; }
}