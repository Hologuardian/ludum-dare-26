package lifebloom.com;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class LifebloomMain extends StateBasedGame
{
	public static final int GAMEPLAY_STATE = 0;
	public static final int LOSE_STATE = 1;
	
	public LifebloomMain() {
		super("Hologuardian - Lifebloom");
	}
	
	public static void main(String[] args) throws SlickException 
	{
		AppGameContainer app = new AppGameContainer(new LifebloomMain());
		  
        app.setDisplayMode(app.getScreenWidth() * 2 / 3, app.getScreenHeight() * 2 / 3, false);
        app.start();
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException 
	{
		this.addState(new GamePlayState(GAMEPLAY_STATE));
		this.addState(new LoseState(LOSE_STATE));
	}

}
